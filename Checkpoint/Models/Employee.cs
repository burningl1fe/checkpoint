﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catel.Data;

namespace Checkpoint.Models
{
    
    public class Employee:ModelBase
    {
        public Employee()
        {
            BirthDate = DateTime.Now;
            HiringDate = DateTime.Now;
            PassportDate = DateTime.Now;
        }

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public Gender Gender { get; set; }
        public DateTime BirthDate { get; set; }
        public DateTime HiringDate { get; set; }
        public string Position { get; set; }
        public string PassportId { get; set; }
        public DateTime PassportDate { get; set; }
        public string PassportIssuedBy { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public byte[] Photo { get; set; }
        public string Description { get; set; }

        public string Login { get; set; }
        public string Password { get; set; }
        
        public string FullName => $"{FirstName} {LastName} {MiddleName}";
    }
}
