﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catel.Data;

namespace Checkpoint.Models
{
    public class Passage:ModelBase
    {
        public long Id { get; set; }

        public long EmployeeId { get; set; }

        public DateTime DateTime { get; set; }

        public bool Entered { get; set; }
    }
}
