﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catel.Data.Repositories;

namespace Checkpoint.Models
{
    public class EmployeeRepository : EntityRepositoryBase<Employee, long>, IEmployeeRepository
    {
        public EmployeeRepository(DbContext dbContext)
            : base(dbContext)
        {
        }
    }

    public interface IEmployeeRepository : IEntityRepository<Employee, long>
    {
    }
}
