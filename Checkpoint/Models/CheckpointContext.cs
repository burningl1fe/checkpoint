﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catel.Data;

namespace Checkpoint.Models
{
    public class CheckpointContext:DbContext
    {
        public DbSet<Employee> Employees { get; set; }

        public DbSet<Passage> Passages { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .IgnoreCatelProperties();
             

            modelBuilder.Entity<Passage>()
                .IgnoreCatelProperties();

        }

    }
}
