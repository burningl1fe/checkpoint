﻿using Catel.Data.Repositories;
using Checkpoint.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkpoint.Repositories
{
    public class EmployeeRepository : EntityRepositoryBase<Employee, int>, IEmployeeRepository
    {
        public EmployeeRepository(DbContext dbContext)
            : base(dbContext)
        {
        }
    }

    public interface IEmployeeRepository : IEntityRepository<Employee, int>
    {
    }
}
