﻿using Catel.Data.Repositories;
using Checkpoint.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Checkpoint.Repositories
{

    public class PassageRepository : EntityRepositoryBase<Passage, int>, IPassageRepository
    {
        public PassageRepository(DbContext dbContext)
            : base(dbContext)
        {
        }
    }

    public interface IPassageRepository : IEntityRepository<Passage, int>
    {
    }
    
}
