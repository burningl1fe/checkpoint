﻿namespace Checkpoint.Views
{
    using Catel.Windows;
    using ViewModels;

    public partial class EmployeeWindow
    {
        public EmployeeWindow()
            : this(null) { }

        public EmployeeWindow(EmployeeViewModel viewModel)
            : base(viewModel)
        {
            InitializeComponent();
        }
    }
}
