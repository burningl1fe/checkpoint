namespace Checkpoint.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        MiddleName = c.String(),
                        Gender = c.Int(nullable: false),
                        BirthDate = c.DateTime(nullable: false),
                        HiringDate = c.DateTime(nullable: false),
                        Position = c.String(),
                        PassportId = c.String(),
                        PassportDate = c.DateTime(nullable: false),
                        PassportIssuedBy = c.String(),
                        Address = c.String(),
                        Phone = c.String(),
                        Email = c.String(),
                        Photo = c.Binary(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Passages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        EmployeeId = c.Long(nullable: false),
                        DateTime = c.DateTime(nullable: false),
                        Entered = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Passages");
            DropTable("dbo.Employees");
        }
    }
}
