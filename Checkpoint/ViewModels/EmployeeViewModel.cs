﻿using System;
using System.ComponentModel;
using Checkpoint.Editor;
using Checkpoint.Models;
using Xceed.Wpf.Toolkit;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;


namespace Checkpoint.ViewModels
{
    using Catel.MVVM;
    using System.Threading.Tasks;




    [CategoryOrder("Личные данные", 1)]
    [CategoryOrder("Служебная информация", 2)]
    [CategoryOrder("Контакты", 3)]
    [CategoryOrder("Паспортные данные", 4)]
    public class EmployeeViewModel : ViewModelBase
    {
        public EmployeeViewModel(Employee employee)
        {
            Employee = employee;
        }

        [Model]
        [Browsable(false)]
        public Employee Employee { get; set; }


        //[Category("Личные данные")]
        //[DisplayName("Фото")]
        //[ViewModelToModel("Employee")]
        //[Editor(typeof(ImageEditor), typeof(ImageEditor))]
        //public string Photo { get; set; }

        [Category("Личные данные")]
        [DisplayName("Фамилия")]
        [ViewModelToModel("Employee")]
        [PropertyOrder(1)]
        public string FirstName { get; set; }

        [Category("Личные данные")]
        [DisplayName("Имя")]
        [PropertyOrder(2)]
        [ViewModelToModel("Employee")]
        public string LastName { get; set; }

        [Category("Личные данные")]
        [DisplayName("Отчество")]
        [ViewModelToModel("Employee")]
        [PropertyOrder(3)]
        public string MiddleName { get; set; }

        [Category("Личные данные")]
        [DisplayName("Пол")]
        [ViewModelToModel("Employee")]
        [PropertyOrder(4)]
        public Gender Gender { get; set; }

        [Category("Личные данные")]
        [DisplayName("Дата рождения")]
        [ViewModelToModel("Employee")]
        [Editor(typeof(DatePickerEditor), typeof(DateTimePicker))]
        public DateTime BirthDate { get; set; }


        [Category("Служебная информация")]
        [DisplayName("Дата найма")]
        [ViewModelToModel("Employee")]
        [Editor(typeof(DatePickerEditor), typeof(DateTimePicker))]
        public DateTime HiringDate { get; set; }

        [Category("Служебная информация")]
        [DisplayName("Должность")]
        [ViewModelToModel("Employee")]
        public string Position { get; set; }

        [Category("Паспортные данные")]
        [DisplayName("Номер")]
        [ViewModelToModel("Employee")]
        public string PassportId { get; set; }

        [Category("Паспортные данные")]
        [DisplayName("Дата выдачи")]
        [ViewModelToModel("Employee")]
        [Editor(typeof(DatePickerEditor), typeof(DateTimePicker))]
        public DateTime PassportDate { get; set; }

        [Category("Паспортные данные")]
        [DisplayName("Кем выдан")]
        [ViewModelToModel("Employee")]
        public string PassportIssuedBy { get; set; }
        [ViewModelToModel("Employee")]

        [Category("Контакты")]
        [DisplayName("Адрес")]
        public string Address { get; set; }
        [ViewModelToModel("Employee")]

        [Category("Контакты")]
        [DisplayName("Телефон")]
        public string Phone { get; set; }
        [ViewModelToModel("Employee")]

        [Category("Контакты")]
        [DisplayName("E-mail")]
        public string Email { get; set; }

        [Category("Данные для входа")]
        [DisplayName("Логин")]
        [ViewModelToModel("Employee")]
        public string Login { get; set; }

        [Category("Данные для входа")]
        [DisplayName("Пароль")]
        [ViewModelToModel("Employee")]
        public string Password { get; set; }

        [DisplayName("Дополнительная информация")]
        [ViewModelToModel("Employee")]
        public string Description { get; set; }

        [Browsable(false)]
        public string FullName => $"{FirstName} {LastName} {MiddleName}";
    }
}
