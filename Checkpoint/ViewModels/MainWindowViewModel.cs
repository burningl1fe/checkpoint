﻿using System.Collections.ObjectModel;
using Catel.Data;
using Catel.Services;
using Checkpoint.Models;

namespace Checkpoint.ViewModels
{
    using Catel.MVVM;
    using System.Threading.Tasks;

    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IUIVisualizerService _uiVisualizerService;
        private readonly IMessageService _messageService;

        public MainWindowViewModel(IUIVisualizerService uiVisualizerService, IMessageService messageService)
        {
            _messageService = messageService;
            _uiVisualizerService = uiVisualizerService;
            Add = new TaskCommand(OnAddExecuteAsync);
            Edit = new TaskCommand(OnEditExecuteAsync, OnEditCanExecute);
            Delete = new TaskCommand(OnDeleteExecuteAsync, OnDeleteCanExecute);
            SearchCommand = new TaskCommand(OnSearchExecuteAsync);
        }

        public override string Title { get { return "Checkpoint"; } }

        public ObservableCollection<Employee> Employees { get; set; }

        public Employee SelectedEmployee { get; set; }

        public TaskCommand Add { get; set; }

        private async Task OnAddExecuteAsync()
        {
            var viewModel = new EmployeeViewModel(new Employee());
            if (await _uiVisualizerService.ShowDialogAsync(viewModel) ?? false)
            {
                using (var uow = new UnitOfWork<CheckpointContext>())
                {
                    var employeeRepository = uow.GetRepository<IEmployeeRepository>();
                    employeeRepository.Add(viewModel.Employee);
                    await uow.SaveChangesAsync();
                }
                Employees.Add(viewModel.Employee);
            }
        }
        public TaskCommand Edit { get; set; }

        /// <summary>
        /// Method to check whether the Edit command can be executed.
        /// </summary>
        /// <returns></returns>
        private bool OnEditCanExecute()
        {
            return (SelectedEmployee != null);
        }

        /// <summary>
        /// Method to invoke when the Edit command is executed.
        /// </summary>
        private async Task OnEditExecuteAsync()
        {
            var viewModel = new EmployeeViewModel(SelectedEmployee);
            if (await _uiVisualizerService.ShowDialogAsync(viewModel) ?? false)
            {
                using (var uow = new UnitOfWork<CheckpointContext>())
                {
                    var employeeRepository = uow.GetRepository<IEmployeeRepository>();
                    employeeRepository.Update(viewModel.Employee);
                    await uow.SaveChangesAsync();
                }
            }
        }

        public TaskCommand Delete { get; set; }

        /// <summary>
        /// Method to check whether the Delete command can be executed.
        /// </summary>
        /// <returns></returns>
        private bool OnDeleteCanExecute()
        {
            return (SelectedEmployee != null);
        }

        /// <summary>
        /// Method to invoke when the Delete command is executed.
        /// </summary>
        private async Task OnDeleteExecuteAsync()
        {
            if (await _messageService.ShowAsync("Are you sure you want to delete this employee?", "Are you sure?", MessageButton.YesNo) == MessageResult.Yes)
            {
                using (var uow = new UnitOfWork<CheckpointContext>())
                {
                    var employeeRepository = uow.GetRepository<IEmployeeRepository>();
                    var employee = employeeRepository.GetByKey(SelectedEmployee.Id);
                    employeeRepository.Delete(employee);
                    await uow.SaveChangesAsync();
                }
                Employees.Remove(SelectedEmployee);
            }
        }


        public TaskCommand SearchCommand { get; set; }
        public string SearchText
        {
            get => _searchText;
            set => _searchText = value;
        }

        string _searchText;

        private async Task OnSearchExecuteAsync()
        {
            using (var uow = new UnitOfWork<CheckpointContext>())
            {
                var employeeRepository = uow.GetRepository<IEmployeeRepository>();
                Employees = new ObservableCollection<Employee>(employeeRepository.GetQuery(x => x.FirstName.Contains(SearchText)));
            }
        }

        // TODO: Register models with the vmpropmodel codesnippet
        // TODO: Register view model properties with the vmprop or vmpropviewmodeltomodel codesnippets
        // TODO: Register commands with the vmcommand or vmcommandwithcanexecute codesnippets

        protected override async Task InitializeAsync()
        {
            await base.InitializeAsync();

            // TODO: subscribe to events here

            using (var uow = new UnitOfWork<CheckpointContext>())
            {
                var employeeRepository = uow.GetRepository<IEmployeeRepository>();
                Employees = new ObservableCollection<Employee>(employeeRepository.GetAll());
            }
        }



        protected override async Task CloseAsync()
        {
            // TODO: unsubscribe from events here

            await base.CloseAsync();
        }
    }
}
