﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using ZXing;
using ZXing.Net.Mobile.Forms;
using ZXing.QrCode;
using PCLCrypto;

namespace Checkpoint.Mobile
{
    public partial class MainPage : ContentPage
    {
        ZXingBarcodeImageView barcode;
        public MainPage()
        {
            InitializeComponent();
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            var login = txtLogin.Text;
            var password = txtPassword.Text;
            var date = DateTime.Now.ToString("d");
            var input = login + password + date;
            var text = Encrypt(input, "secretkey!");

            barcode = new ZXingBarcodeImageView
            {
                HorizontalOptions = LayoutOptions.FillAndExpand,

                VerticalOptions = LayoutOptions.FillAndExpand,
            };
            barcode.BarcodeFormat = BarcodeFormat.QR_CODE;
            barcode.BarcodeOptions = new QrCodeEncodingOptions
            {
                Height = 480,
                Width = 480
            };
            barcode.BarcodeValue = text;
            Content = barcode;
        }

        string Encrypt(string input, string key)
        {
            var mac = WinRTCrypto.MacAlgorithmProvider.OpenAlgorithm(MacAlgorithm.HmacSha1);
            var binaryKey = WinRTCrypto.CryptographicBuffer.ConvertStringToBinary(key, Encoding.UTF8);
            var cryptoKey = mac.CreateKey(binaryKey);
            var hash = WinRTCrypto.CryptographicEngine.Sign(cryptoKey, WinRTCrypto.CryptographicBuffer.ConvertStringToBinary(input, Encoding.UTF8));
            return WinRTCrypto.CryptographicBuffer.EncodeToBase64String(hash);
        }


    }
}
